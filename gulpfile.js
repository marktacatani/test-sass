var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');
var minifyCss = require('gulp-minify-css');
var util = require('gulp-util');

console.log(util.env.production);

var config = {
    bootstrapDir: '../bower_components/bootstrap-sass',
    publicDir: './public',
    sassSrc: './css/**/*.scss',
    production: !!util.env.production
};

gulp.task('css', function() {
    gulp.src(config.sassSrc)
        .pipe(sourcemaps.init())
        .pipe(sass({
            includePaths: [config.bootstrapDir + '/assets/stylesheets'],
        }))
        .pipe(concat('main.css'))
        .pipe(config.production ? minifyCss() : util.noop())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(config.publicDir + '/css'));
});

gulp.task('fonts', function() {
    gulp.src(config.bootstrapDir + '/assets/fonts/**/*')
        .pipe(gulp.dest(config.publicDir + '/fonts'));
});

gulp.task('watch', function() {
     gulp.watch(config.sassSrc, ['css', 'fonts'])
});

gulp.task('default', ['css', 'fonts', 'watch']);
//gulp.task('default', ['css']);
